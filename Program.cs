﻿using System.Globalization;
using System.Text.Json;

string apiKey = "6f7df5ca-8ba6-4a82-93c9-7a2657c5edc9";

int interval;
string json1, json2;

// AirApp
Console.WriteLine("Введите временной интервал в минутах от 30 минут до 4 часов (240 мин):");

while (true)
{
    string input = Console.ReadLine()!;
    int.TryParse(input, out interval);
    if (interval >= 30 && interval <= 240 && interval % 5 == 0)
        break;
    Console.WriteLine("Повторите ввод. Число должно быть от 30 до 240 минут и кратно 5!");
}

if (File.Exists("rasp1.txt") && File.Exists("rasp2.txt"))
{
    Console.WriteLine($"Обнаружены файлы расписания от {DateOnly.FromDateTime(File.GetCreationTime("rasp1.txt"))}, данные будут считаны с него");
    json1 = File.ReadAllText("rasp1.txt");
    json2 = File.ReadAllText("rasp2.txt");
}
else
{
    Console.WriteLine("Обновляем данные");
    var Uri = $"https://api.rasp.yandex.net/v3.0/schedule/?apikey={apiKey}&station=s9600213&transport_types=plane&event=arrival&limit=1000";
    try
    {
        using (var client = new HttpClient())
        {
            json1 = await client.GetStringAsync(Uri + "&date=" + DateTime.UtcNow.ToString("o", CultureInfo.InvariantCulture));
            json2 = await client.GetStringAsync(Uri + "&date=" + DateTime.UtcNow.AddDays(1).ToString("o", CultureInfo.InvariantCulture));
            File.WriteAllText("rasp1.txt", json1);
            File.WriteAllText("rasp2.txt", json2);
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex);
        Console.WriteLine("Ошибка при обновлении данных! Перезапустите приложение чтобы попробовать ещё раз");
        return;
    }
}

var data1 = JsonSerializer.Deserialize<Root>(json1);
var data2 = JsonSerializer.Deserialize<Root>(json2);
if (data1 is null || data2 is null)
{
    Console.WriteLine("Ошибка десериализации данных!");
    return;
}

const int intervalsPerDay = 60 / 5 * 24; // 60m / 5m interval * 24h
var ranges = new int[intervalsPerDay * 2];
foreach (var item in data1.schedule)
{
    int rng_idx = (int)TimeOnly.FromDateTime(DateTime.Parse(item.arrival)).ToTimeSpan().TotalMinutes / 5;
    ranges[rng_idx]++;
}
foreach (var item in data2.schedule)
{
    int rng_idx = (int)TimeOnly.FromDateTime(DateTime.Parse(item.arrival)).ToTimeSpan().TotalMinutes / 5;
    ranges[rng_idx + intervalsPerDay]++;
}

int minEmpty = 0, maxCountEmpty = -1, startEmpty = 0, countEmpty = 0;
int startFly = 0, maxCountFly = int.MaxValue, countFly = 0, intervalFly = interval / 5;

for (int i = 0; i < ranges.Length; i++)
{
    if (ranges[i] == 0)
    {
        if (countEmpty == 0)
            startEmpty = i;

        if (maxCountEmpty < countEmpty)
        {
            minEmpty = startEmpty;
            maxCountEmpty = countEmpty;
        }

        countEmpty++;

#if DEBUG
        Console.WriteLine("В это время рейсов нет: " + TimeSpan.FromMinutes(i * 5));
#endif
    }
    else
    {
        countEmpty = 0;
    }

    countFly += ranges[i];
    if (i > intervalFly)
    {
        countFly -= ranges[i - intervalFly];
        if (countFly < maxCountFly &&         // Наименьшее количество перелётов в интервале
            countFly > 0 &&                   // В нём должен быть хоть 1 перелёт
            ranges[i - intervalFly + 1] > 0)  // Начало интервала это существующий рейс
        {
            maxCountFly = countFly;
            startFly = i - intervalFly + 1;
        }
    }
}

#if DEBUG
Console.WriteLine($"Насчитал количество перелетов в последнем интервале ({countFly}): {IntervalToDate(ranges.Length - intervalFly)} - {IntervalToDate(ranges.Length)}");
#endif
Console.WriteLine($"\r\nНаименьшее количество прибытий в интервале: {IntervalToDate(startFly)} - {IntervalToDate(startFly + intervalFly)}\r\n");
Console.WriteLine($"Наибольший интервал без прилёта рейсов: {IntervalToDate(minEmpty)} - {IntervalToDate(minEmpty + maxCountEmpty)}");

static DateTime IntervalToDate(int intervals)
{
    return DateOnly.FromDateTime(DateTime.Now).ToDateTime(TimeOnly.MinValue).Add(TimeSpan.FromMinutes(intervals * 5));
}

static TimeSpan IntervalToTime(int intervals)
{
    return TimeSpan.FromMinutes(intervals % intervalsPerDay * 5);
}

public class Carrier
{
    public int code { get; set; }
    public string title { get; set; }
    public Codes codes { get; set; }
}

public class Codes
{
    public string sirena { get; set; }
    public string iata { get; set; }
    public string icao { get; set; }
}

public class Pagination
{
    public int total { get; set; }
    public int limit { get; set; }
    public int offset { get; set; }
}

public class Root
{
    public object date { get; set; }
    public Station station { get; set; }
    public string @event { get; set; }
    public Pagination pagination { get; set; }
    public List<Schedule> schedule { get; set; }
    public List<object> interval_schedule { get; set; }
}

public class Schedule
{
    public Thread thread { get; set; }
    public string terminal { get; set; }
    public bool is_fuzzy { get; set; }
    public string stops { get; set; }
    public string platform { get; set; }
    public object except_days { get; set; }
    public string arrival { get; set; }
    public object departure { get; set; }
    public string days { get; set; }
}

public class Station
{
    public string type { get; set; }
    public string title { get; set; }
    public object short_title { get; set; }
    public object popular_title { get; set; }
    public string code { get; set; }
    public string station_type { get; set; }
    public string station_type_name { get; set; }
    public string transport_type { get; set; }
}

public class Thread
{
    public string number { get; set; }
    public string title { get; set; }
    public string short_title { get; set; }
    public Carrier carrier { get; set; }
    public string vehicle { get; set; }
    public string transport_type { get; set; }
    public object express_type { get; set; }
    public TransportSubtype transport_subtype { get; set; }
    public string uid { get; set; }
}

public class TransportSubtype
{
    public object title { get; set; }
    public object code { get; set; }
    public object color { get; set; }
}